package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.domain.builder.CustomerBuilder;
import br.edu.unisep.store.domain.dto.RegisterCustomerDto;
import br.edu.unisep.store.domain.validator.CustomerValidator;

public class RegisterCustomerUseCase {

    public void execute(RegisterCustomerDto registerCustomer) {
        var validator = new CustomerValidator();
        validator.validate(registerCustomer);

        var builder = new CustomerBuilder();
        var customer = builder.from(registerCustomer);

        var dao = new CustomerDao();
        dao.save(customer);
    }

}

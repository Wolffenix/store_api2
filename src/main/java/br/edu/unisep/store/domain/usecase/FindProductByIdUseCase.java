package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.data.dao.ProductDao;
import br.edu.unisep.store.domain.builder.CustomerBuilder;
import br.edu.unisep.store.domain.builder.ProductBuilder;
import br.edu.unisep.store.domain.dto.CustomerDto;
import br.edu.unisep.store.domain.dto.ProductDto;
import br.edu.unisep.store.domain.validator.FindCustomerByIdValidator;

public class FindProductByIdUseCase {

    public ProductDto execute(Integer id) throws IllegalArgumentException {

        var dao = new ProductDao();
        var product = dao.findById(id);

        var builder = new ProductBuilder();
        return builder.from(product);
    }
}

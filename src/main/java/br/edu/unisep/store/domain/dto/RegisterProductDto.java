package br.edu.unisep.store.domain.dto;


import lombok.Data;

@Data
public class RegisterProductDto {

    private String name;
    private String description;
    private Float price;
    private String brand;
    private Integer status;

}

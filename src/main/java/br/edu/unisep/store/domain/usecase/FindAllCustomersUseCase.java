package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.CustomerDao;
import br.edu.unisep.store.domain.builder.CustomerBuilder;
import br.edu.unisep.store.domain.dto.CustomerDto;

import java.util.List;

public class FindAllCustomersUseCase {

    public List<CustomerDto> execute() {
        var dao = new CustomerDao();
        var customers = dao.findAll();

        var builder = new CustomerBuilder();
        return builder.from(customers);
    }

}

package br.edu.unisep.store.domain.validator;

import br.edu.unisep.store.domain.dto.RegisterCustomerDto;
import br.edu.unisep.store.domain.dto.RegisterProductDto;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.Validate;

import java.util.Arrays;

public class ProductValidator {

    public void validate(RegisterProductDto registerProduct) {
        Validate.notBlank(registerProduct.getName(), "Informe o nome do produto!");
        Validate.notBlank(registerProduct.getDescription(), "Informe a descrição do produto!");
        Validate.notNull(registerProduct.getPrice(), "Informe o preço do produto!");
        Validate.isTrue(registerProduct.getPrice() >= 0, "O preço do produto não pode ser menor que 0.00!");
        Validate.notBlank(registerProduct.getBrand(), "Informe a marca do produto!");
        Validate.notNull(registerProduct.getStatus(), "Informe o status do produto!");
        Validate.isTrue(validStatus(registerProduct.getStatus()), "Status do produto invalido!");
    }

    private boolean validStatus(Integer status){
        return !(status != 0 && status != 1 && status != 2);
    }

}
